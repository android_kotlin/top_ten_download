package ch.anmanosca.top10downloader

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.TextView
import kotlinx.android.synthetic.main.list_record.view.*
import java.net.URL
import kotlin.properties.Delegates

/**
 * Created by ansc on 20.03.18.
 */

class ViewHolder(v: View) {
    val tvName: TextView = v.findViewById(R.id.tvName)
    val tvArtist: TextView = v.findViewById(R.id.tvArtist)
    val tvSummary: TextView = v.findViewById(R.id.tvSummary)
    val ibImage: ImageButton = v.findViewById(R.id.ibImage)
}

class FeedAdapter(context: Context, private val resource: Int, private val applications: List<FeedEntry>)
    : ArrayAdapter<FeedEntry>(context, resource) {

    private val inflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return applications.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            view = inflater.inflate(resource, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

//        val tvName: TextView = view.findViewById(R.id.tvName)
//        val tvArtist: TextView = view.findViewById(R.id.tvArtist)
//        val tvSummary: TextView = view.findViewById(R.id.tvSummary)

        val currentApp = applications[position]
        val downloadImage = DownloadImage(viewHolder.ibImage)

        viewHolder.tvName.text = currentApp.name
        viewHolder.tvArtist.text = currentApp.artist
        viewHolder.tvSummary.text = currentApp.summary
        downloadImage.execute(currentApp.imageURL)
        viewHolder.ibImage.setOnClickListener({

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(currentApp.link))
            context.startActivity(intent)
        })

        return view
    }

    companion object {
        private class DownloadImage(imageButton: ImageButton) : AsyncTask<String, Void, Bitmap>() {
            private val TAG = "DownloadImage"

            var propImageView : ImageButton by Delegates.notNull()

            init {
                propImageView = imageButton
            }

            override fun onPostExecute(result: Bitmap) {
                super.onPostExecute(result)
                propImageView.ibImage.setImageBitmap(result)
            }

            override fun doInBackground(vararg url: String?): Bitmap {
                Log.d(TAG, "doInBackground: starts with ${url[0]}")
                val inputStream = URL(url[0]).openStream()

                return BitmapFactory.decodeStream(inputStream)
            }
        }
    }
}